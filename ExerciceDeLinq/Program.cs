﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

using LINQDataContext;

namespace ExerciceDeLinq
{
    class Program
    {
        static void Main(string[] args)
        {
            DataContext dc = new DataContext();


            #region 2.1
            Console.WriteLine("Ex 2.1");
            dc.Students
                .Select(s => new { s.Last_Name, s.Login, s.Year_Result, s.BirthDate })
                .Print();
            #endregion

            #region 2.2
            Console.WriteLine("Ex 2.2");
            dc.Students.Select(s => new
            {
                NomComplet = s.Last_Name + " " + s.First_Name,
                s.Student_ID,
                s.BirthDate
            }).Print();
            #endregion

            #region 2.3
            Console.WriteLine("Ex 2.3");
            IEnumerable<string> l = dc.Students.Select(s => 
                string.Join(
                    "|", 
                    s.GetType().GetProperties().Select(p => p.GetValue(s, null).ToString())
                )
            );
            Console.WriteLine(string.Join("\n", l));
            #endregion

            #region Demo Where Order By
            Console.WriteLine("Demo Where");
            dc.Students
                .Where(s => s.Year_Result > 10)  
                .OrderByDescending(s => s.Year_Result)
                .ThenBy(s => s.Last_Name)
                .Select(s => new { 
                    s.Last_Name, 
                    s.Year_Result,
                })
                .Print();
            // attention à eviter car il vaut mieux executer le where avant
            //dc.Students
            //    .Select(s => new { s.Last_Name, s.Year_Result })
            //    .Where(s => s.Year_Result > 10);
            #endregion

            #region 3.1
            Console.WriteLine("\n3.1");
            dc.Students.Where(s => s.BirthDate.Year < 1955)
                .Select(s => new {
                    s.Last_Name,
                    s.Year_Result,
                    Statut = (s.Year_Result >= 12) ? "Ok" : "Ko"
                }).Print();
            #endregion

            #region 3.2
            Console.WriteLine("\n3.2");
            dc.Students.Where(s => s.BirthDate.Year > 1955 && s.BirthDate.Year < 1965)
                .Select(s => new { 
                    s.Last_Name,
                    s.Year_Result,
                    Categorie = GetCat(s.Year_Result)
                }).Print();
            #endregion

            #region 3.3
            Console.WriteLine("\n3.3");
            dc.Students.Where(s => s.Last_Name.EndsWith("r"))
                .Select(s => new
                {
                    s.Last_Name, s.Section_ID
                }).Print();
            #endregion

            #region 3.4
            Console.WriteLine("\n3.4");
            dc.Students.Where(s => s.Year_Result <= 3)
                .Select(s => new
                {
                    s.Last_Name,
                    s.Year_Result
                })
                .OrderByDescending(s => s.Year_Result)
                .Print();
            #endregion

            #region 3.5
            Console.WriteLine("\n3.5");
            dc.Students.Where(s => s.Section_ID == 1110)
                .Select(s => new
                {
                    FullName = s.Last_Name + " " + s.First_Name,
                    s.Year_Result
                })
                .OrderBy(s => s.FullName)
                .Print();
            #endregion

            #region 3.6
            Console.WriteLine("\n3.6");
            dc.Students.Where(s => s.Section_ID == 1110 && (s.Year_Result < 12 || s.Year_Result > 18))
                .Select(s => new
                {
                    s.Last_Name,
                    s.Section_ID,
                    s.Year_Result
                })
                .OrderBy(s => s.Section_ID)
                .Print();
            #endregion

            #region 3.7
            Console.WriteLine("\n3.7");
            dc.Students.Where(s => s.Section_ID.ToString().StartsWith("13") && s.Year_Result <= 12)
                .Select(s => new
                {
                    s.Last_Name,
                    s.Section_ID,
                    Result_100 = s.Year_Result *5
                })
                .OrderByDescending(s => s.Result_100)
                .Print();
            #endregion

            #region Demo Aggregation
            Console.WriteLine("\nCount");
            Console.WriteLine(dc.Students.Where(s => s.Year_Result < 10).Count());

            Console.WriteLine("\nMoyenne");
            Console.WriteLine(dc.Students.Where(s => s.Year_Result < 10).Average(s => s.Year_Result));

            Console.WriteLine("\nMax");
            Console.WriteLine(dc.Students.Where(s => s.Year_Result < 10).Max(s => s.BirthDate));

            Console.WriteLine("\nMin");
            Console.WriteLine(dc.Students.Where(s => s.Year_Result < 10).Min(s => s.BirthDate));

            Console.WriteLine("\nSomme");
            Console.WriteLine(dc.Students.Where(s => s.Year_Result < 10).Sum(s => s.Year_Result));

            Student st = dc.Students.Single(s => s.BirthDate == dc.Students.Max(s1 => s1.BirthDate));

            //Student st1 = dc.Students.First(s => s.Year_Result == 0);
            // Student st2 = dc.Students.FirstOrDefault(s => s.Year_Result == 0);
            //Student st3 = dc.Students.Single(s => s.Year_Result == 0);
            //Student st4 = dc.Students.SingleOrDefault(s => s.Year_Result == 0);

            Console.WriteLine("\nGroup");
            // Grouper les éléments sur une colonne
            List<IGrouping<int, Student>> groupList = dc.Students.GroupBy(s => s.Section_ID).ToList();

            //IGrouping<int,Student> firstGroup = groupList.First();
            //Console.WriteLine("section = " + firstGroup.Key);
            //firstGroup.Print();

            //foreach (IGrouping<int, Student> group in groupList)
            //{
            //    Console.WriteLine("section = " + group.Key);
            //    group.Print();
            //}
            groupList.ForEach(g => { Console.WriteLine(g.Key); g.Print(); });
            #endregion

            #region Take / Skip
            // Skip => saute un certain nombre d'éléments
            // Take => prends un certain nombre d'éléments
            //Console.WriteLine("\ntake/skip");
            //var l42 = dc.Students.Take(5).ToList();
            //l42.AddRange(dc.Students.Skip(10).Take(5));
            //l42.Print();
            #endregion

            #region Jointure
            Console.WriteLine("\nJointures");
            // liste de départ
            dc.Students.Join(
                // liste que l'on souhaite joindre
                dc.Sections,
                // propriété qui sera comparée dans la première liste
                st1 => st1.Section_ID,
                // propriété qui sera comparée dans la seconde liste
                se => se.Section_ID,
                // resultat que l'on veut obtenir sur base d'UN element de la première liste et d'UN élément de la seconde
                (st1, se) => new { 
                    st1.Last_Name,
                    se.Section_Name
                }).Print();
            #endregion

            #region GroupJoin
            Console.WriteLine("\nGroupJoin");
            // liste de départ
            var result = dc.Sections.GroupJoin(
                // liste que l'on souhaite joindre
                dc.Students,
                // propriété qui sera comparée dans la première liste
                se => se.Section_ID,
                // propriété qui sera comparée dans la seconde liste
                stud => stud.Section_ID,
                // resultat qu'on veut obtenir sur base d'UN élément de la première liste et de la LISTE des éléments
                // associès à l' élément de la premère liste
                (se, students) => new
                {
                    se.Section_ID,
                    se.Section_Name,
                    Students = students.Select(stud => new { stud.Last_Name, stud.First_Name }),
                    AverageResult = students.Average(stud => stud.Year_Result)
                }
            ); ;

            foreach (var se in result)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(se.Section_Name);
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine(se.AverageResult);
                Console.ResetColor();
                foreach (var stu in se.Students)
                {
                    Console.WriteLine(stu.Last_Name + " " + stu.First_Name);
                }
            }
            #endregion


            #region
            Console.WriteLine("\nEx 5.8");
            dc.Sections
                .Join(
                dc.Professors, 
                se => se.Section_ID, 
                p => p.Section_ID, 
                (se, p) => new { se.Section_ID, se.Section_Name, p.Professor_Name }
            ).Print();

            Console.WriteLine("\nEx 5.7");

            // Left join
            dc.Sections.GroupJoin(
                dc.Professors,
                se => se.Section_ID,
                p => p.Section_ID,
                (se, profs) => new { se.Section_ID, se.Section_Name, profs }
            ).SelectMany(
                sectionProfs => sectionProfs.profs.DefaultIfEmpty(), 
                (se, p) => new { 
                se.Section_ID,
                p?.Professor_Name,
                se.Section_Name
            }).Print();
            #endregion



            #region Console.ReadLine()
            Console.ReadLine();
            #endregion
        }

        //private static string CalculateGrade(int year_Result, List<Grade> grades)
        //{
        //    return grades.FirstOrDefault(g => g.Lower_Bound <= year_Result && g.Upper_Bound >= year_Result).GradeName;
        //}

        private static string GetCat(int year_Result)
        {
            if (year_Result < 12) return "Inf";
            else if (year_Result > 12) return "Sup";
            else return "Neutre";
        }
    }

    static class Extensions
    {
        public static void Print<T>(this IEnumerable<T> list)
        {
            // sans Linq
            //foreach (T item in list)
            //{
            //    PropertyInfo[] properties = typeof(T).GetProperties();
            //    foreach (PropertyInfo p in properties)
            //    {
            //        Console.Write(p.GetValue(item, null) + " - ");
            //    }
            //    Console.WriteLine();
            //}

            // avec linq
            Console.WriteLine(string.Join("\n", list.Select(item =>
                string.Join(
                    " - ",item.GetType().GetRuntimeProperties().Select(p => p.GetValue(item, null))
                ))
            ));
        }
    }
}
